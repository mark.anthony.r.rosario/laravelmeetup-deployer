# Set Deploy Bot permissions
```
# since ubuntu will act as the “deploybot”  allow “ubuntu” to manipulate the webdirectory
sudo chown ubuntu:ubuntu /usr/share/nginx/html/ -Rf
```

# php-fpm restart and change-ownership permissions
the "deploy bot" user should be allowed to use sudo to perform these
```
# switch to root and use visudo to tweak sudo prvileges
# this allows "mark" to run "sudo /usr/sbin/service php7.2-fpm reload" without password and set directory ownership
mark ALL=NOPASSWD: /usr/sbin/service php7.2-fpm reload, /bin/chown
```
