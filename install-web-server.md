# Install php + php-fpm + nginx
since we will be using an ubuntu18:04 server this will install php7.2, php7.2-fpm, nginx 1.14.0
```
# switch to root to perform operations below
sudo -s
```

```
# update repositories
apt update

# install php/fpm/nginx/extentions
apt install -y php nginx php-fpm php7.2-mbstring

# remove the default apache2 installation because we will be using nginx
service apache2 stop && apt remove apache2

# copy the predefined nginx configs
cp ./server/etc/nginx/nginx.conf /etc/nginx/nginx.conf
cp ./server/etc/nginx/nginx.conf /etc/nginx/nginx.conf
rm /etc/nginx/sites-enabled/default.conf
cp server/etc/nginx/sites-enabled/laravelmeetup.conf /etc/nginx/sites-enabled/laravelmeetup.conf

# start our web services
service nginx start

service php7.2-fpm start

# optional: create hello world page
echo "<h1> Hello from Salarium </h1>" > /var/www/html/index.html
```

