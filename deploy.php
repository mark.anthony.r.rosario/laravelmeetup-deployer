<?php

# this script has been generated using` dep init -t Laravel `
# this used the deployer laravel recipe
# see: https://github.com/deployphp/deployer/blob/master/recipe/laravel.php


namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'laravelmeetup');

// Project repository
set('repository', 'git@gitlab.com:mark.anthony.r.rosario/laravel5-example.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true); 

// Shared files/dirs between deploys 
// datastores that should persist between deploys
// user uploads, caches, session files, .env files, log files
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server 
// for things like uploads, temporary files
add('writable_dirs', []);


// Hosts
// ensure you have access to this server via ssh
// you may use `ssh-copy-id` to setup passwordless ssh access to your "production" servers or add your "deployer' server's public key to the authorized_keys of the "production" server
// you can have multiple hosts (local,development,staging,uat,production)
host('localhost')
    ->stage('local')
    ->user('mark')
    ->set('deploy_path', '/usr/share/nginx/html/{{application}}');    

# normally you put a domain name her
# this server has been preconfigured with nginx + php7.2 + php7.2-fpm
host('54.179.128.103')
    ->stage('production')
    ->user('ubuntu')
    ->roles('app')
    ->identityFile('~/.ssh/laravelmeetup.pem')
    ->set('deploy_path', '/usr/share/nginx/html/{{application}}');    

    
// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

// before('deploy:symlink', 'artisan:migrate');

// add custom tasks
before('artisan:config:cache', 'artisan:key:generate');
desc('artisan:key:generate');
task('artisan:key:generate', function () {
    run('cd {{release_path}} && php artisan key:generate');
});

after('deploy:symlink', 'php-fpm:reload');
desc('php-fpm:reload');
task('php-fpm:reload', function () {
    run('sudo /usr/sbin/service php7.2-fpm reload');
});

after('php-fpm:reload', 'set-permissions');
after('php-fpm:reload', 'set-permissions-local');

desc('set-permissions-local');
task('set-permissions-local', function () {
    run('sudo chown mark:www-data /usr/share/nginx/html/laravelmeetup -Rf && sudo chown mark:www-data /usr/share/nginx/html/laravelmeetup/releases/ -Rf');
})->onStage('local');

desc('set-permissions');
task('set-permissions', function () {
    run('sudo chown ubuntu:www-data /usr/share/nginx/html/laravelmeetup -Rf && sudo chown ubuntu:www-data /usr/share/nginx/html/laravelmeetup/releases/ -Rf');
})->onStage('production');