# laravelmeetup-deployer

zero downtime Laravel deployments using deployer.org
prepared for laravelPH X AWS User Group PH meetup at @Salarium
see slides: https://drive.google.com/file/d/1DqOPL7QicN7ZH4nAuuccR3BR_KPKIfp1/view?usp=sharing

# to deploy 
```bash
# deploy to local machine
bash deploy-local.sh

# deploy to remote machine
bash deploy-production.sh
```

# to rollback
```bash
# rollback local machine by 1 version
bash rollback-local.sh

# rollback remote machine by 1 version
bash rollback-production.sh
```